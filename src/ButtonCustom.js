import React from 'react';
import PropTypes from 'prop-types';
import './Button.css';

ButtonCustom.propTypes = {
	content: PropTypes.string || PropTypes.element,
	rounded: PropTypes.bool,
    needSpin : PropTypes.bool
};

function ButtonCustom({ content = '' , rounded= false , needSpin = false , randomNumber}) {

	return (
		<button className={`btn_custom ${rounded && "btn_rounded"} ${needSpin && 'btn_spin'} `} onClick={randomNumber}>
			{content}
		</button>
	);
}

export default ButtonCustom;
