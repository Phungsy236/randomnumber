import React, {useState} from 'react';
import PropTypes from 'prop-types';
import './Input.css'

InputCustom.propTypes = {
    getValue: PropTypes.func.isRequired
};

function InputCustom({getValue}) {

    function handleGetValue(e) {
        getValue(e.target.value)
    }

    function handleValidate(event) {
        const keyCode = event.keyCode || event.which;
        const keyValue = String.fromCharCode(keyCode);
        if(keyCode<48 || keyCode>57 || isNaN(keyValue)){
            event.preventDefault()
        }
        // console.log(keyCode , keyValue)
    }

    return (
        <input onChange={handleGetValue} onKeyPress={handleValidate} className={'input_custom'} type={'number'}></input>
    );
}

export default InputCustom;
